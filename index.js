const log = message => {
	console.log(message);
}

// log("Hello world")

// [SECTION] Assignment Operator
	// Basic Assignment Operator (=)
	// The assignemnt operator assigns the value of the right hand operand to a variable
	let assignmentNumber = 8;
	log("The value of the assignment Number variable: " + assignmentNumber);
	log("---------------------");

// [SECITON] Arithmetic Operations
	
	let x = 200;
	let y = 18;
	log("x : " + x);
	log("y : " + y);
	log("---------------------");

	// Addition
	let sum = x + y;
	log("Result of addition operator: " + sum);

	// Subtraction
	let difference = x - y;
	log("Result of subtraction operator: " + difference);

	// Multiplication
	let product = x * y;
	log("Result of multiplication operator: " + product);

	// Division
	let quotient = x / y;
	log("Result of division operator: " + quotient);

	// Modulo
	let remainder = x % y;
	log("Result of modulo operator: " + remainder);

// Continuation of assignment operator

// Addition Assignment Operator
	// assignmentNumber = assignmentNumber + 2; //long method
	assignmentNumber += 2; //short method
	log("Result of addition assignment operator: " + assignmentNumber); //assignmentNumber value is 10


// Subtraction Assignment Operator
	// assignmentNumber = assignmentNumber - 3; //long method
	assignmentNumber -= 3; //short method
	log("Result of subtractio assignment operator: " + assignmentNumber);

// Multiplication Assignment Operator
	assignmentNumber *= 2; //short method
	log("Result of multiplication assignment operator: " + assignmentNumber);

// Division Assignment Operator
	assignmentNumber /= 2; //short method
	log("Result of division assignment operator: " + assignmentNumber);



// [SECTION] PEMDAS (Order of Operations)
// Multiple Operators and Parenthesis


let mdas = 1 + 2 - 3 * 4 / 5; // 3 - 12/5

log(mdas);

let pemdas = (1+(2-3)) * (4/5);

log(pemdas);

// [SECTION] Increment and Decrement

// Operators taht add or subtract values by 1 and reassign the value of the variable where the increment(++)/decrement(--) was applied.

let z = 1;

// preincrement
let increment = ++z; 

log("Result of pre-increment: " + increment); // 2
log("Result of pre-increment of z: " + z); // 2

// post increment
increment = z++;
log("Result of post-increment: " + increment);
log("Result of post-increment of z: " + z);


// pre-decrement
let decrement = --z;
log("Result of pre-decrement: " + decrement);
log("Result of pre-decrement of z: " + z);

// post-decrement
decrement = z--;
log("Result of post-decrement: " + decrement);
log("Result of post-decrement of z: " + z);


// [SECTION] Type Coercion
// is the automatic conversion of values from one data type to another

	// combination of number and string data type will result to string.
	let numA = 10;
	let numB = "12";

	let coercion = numA + numB;
	log(coercion);
	log(typeof coercion);


	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;
	log(nonCoercion);
	log(typeof nonCoercion);

	coercion = numB + numA;
	log(coercion);

	// combinaiton number and boolean data type will result to number
	let numX = 10;
	let numY = true;

	coercion = numX + numY;
	log(coercion);
	log(typeof coercion);

	let num1 = 1;
	let num2 = false;
	coercion = num1 + num2;
	log(coercion);
	log(typeof coercion);


// [SECTION] Comparison Operators
// Comparison operators are used to evaluate and compare the left and right operands
// After evaluation, it returns a boolean value

// equality operator
log("---------------------");
log(1 == 1); //true
log(1 == 2); //false 
log(1 == '1'); //true
log( 0 == false); //true
log( 1 == true); //true

// With comparison operators, the data type is always coerced to be a number with exceptions of course
log("---------------------");
let juan = "juan";
log('juan' == 'juan');
log('juan' == 'Juan');
log(juan == "juan");

// inequality operator (!=)
log("---------------------");
log(1 != 1); //false
log(1 != 2); //true
log(1 != '1'); //false
log( 0 != false); //false
log( 1 != true); //false
log('juan' != 'juan');
log('juan' != 'Juan');
log(juan != "juan");
log("---------------------");
// [SECTION] Relational Operators
// Some comparison operators to check wether one value is greater or less than the other value

let a = 50;
let b = 65; 

// GT or Greater Than Operator (>)
let isGreaterThan = a > b;
log(isGreaterThan);

// LT or Less Than Operator (<)
let isLessThan = a < b;
log(isLessThan);

// GTE or Greater Than or Equal (=>)
let isGTorEqual = a >= b;
log(isGTorEqual);



// Logical AND Operator (&&)
let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
log("Results of logical AND operator: " + allRequirementsMet);


// Logical OR Operator (||)
someRequirementsMet = isLegalAge || isRegistered;
log("Results of logical AND operator: " + someRequirementsMet);

// Logical NOT Operator (!)
// Returns the opposite value
let someRequirementsNotMet = !isLegalAge;
log("Result of logical NOT Operator: " + someRequirementsNotMet);
